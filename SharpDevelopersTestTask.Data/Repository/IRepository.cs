﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SharpDevelopersTestTask.Core.Domain;

namespace SharpDevelopersTestTask.Data.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        #region Properties

        DbSet<T> Entities { get; }

        #endregion

        #region Methods

        T Get(int id);

        IEnumerable<T> GetAll();

        void Insert(T entity);

        void Update(T entity);

        void Delete(T entity);

        #endregion
    }
}