﻿using Microsoft.EntityFrameworkCore;
using SharpDevelopersTestTask.Core.Domain;

namespace SharpDevelopersTestTask.Data
{
    public class ApplicationContext : DbContext, IDbContext
    {
        #region Constructor

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Properties

        public DbSet<User> Users { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        #endregion

        #region Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=Victor;Initial Catalog=SharpDevelopersTestTask;Integrated Security=True;");
        }

        public new DbSet<T> Set<T>() where T : BaseEntity
        {
            return base.Set<T>();
        }

        #endregion
    }
}