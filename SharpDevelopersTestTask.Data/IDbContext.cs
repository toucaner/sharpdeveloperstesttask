﻿using Microsoft.EntityFrameworkCore;
using SharpDevelopersTestTask.Core.Domain;

namespace SharpDevelopersTestTask.Data
{
    public interface IDbContext
    {
        #region Methods

        DbSet<T> Set<T>() where T : BaseEntity;

        int SaveChanges();

        #endregion
    }
}