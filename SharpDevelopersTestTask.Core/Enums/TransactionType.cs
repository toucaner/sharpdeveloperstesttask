﻿namespace SharpDevelopersTestTask.Core.Enums
{
    public enum TransactionType
    {
        Credit,
        Debit
    }
}