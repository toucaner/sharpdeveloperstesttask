﻿using System.ComponentModel.DataAnnotations;

namespace SharpDevelopersTestTask.Core.Domain
{
    public class BaseEntity
    {
        #region Properties

        [Key]
        public int Id { get; set; }

        #endregion
    }
}