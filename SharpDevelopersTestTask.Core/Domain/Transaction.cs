﻿using System;

namespace SharpDevelopersTestTask.Core.Domain
{
    public class Transaction : BaseEntity
    {
        #region Properties

        public DateTime Created { get; set; }

        public decimal Amount { get; set; }

        public Account Creditor { get; set; }

        public Account Debitor { get; set; }

        #endregion
    }
}