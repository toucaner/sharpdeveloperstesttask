﻿namespace SharpDevelopersTestTask.Core.Domain
{
    public class Account : BaseEntity
    {
        #region Properties

        public decimal Amount { get; set; }

        public User User { get; set; }

        #endregion
    }
}