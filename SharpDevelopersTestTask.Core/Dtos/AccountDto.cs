﻿using SharpDevelopersTestTask.Core.Domain;

namespace SharpDevelopersTestTask.Core.Dtos
{
    public class AccountDto
    {
        #region Properties

        public int Id { get; set; }

        public decimal Amount { get; set; }

        public User User { get; set; }

        #endregion
    }
}