﻿namespace SharpDevelopersTestTask.Core.Dtos
{
    public class UserDto
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string PasswordConfirm { get; set; }

        #endregion
    }
}