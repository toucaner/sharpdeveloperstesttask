﻿using System;
using SharpDevelopersTestTask.Core.Domain;

namespace SharpDevelopersTestTask.Core.Dtos
{
    public class TransactionDto
    {
        #region Properties

        public int Id { get; set; }

        public DateTime Created { get; set; }

        public decimal Amount { get; set; }

        public string Creditor { get; set; }

        public string Debitor { get; set; }

        public int CreditorId { get; set; }

        public int DebitorId { get; set; }

        #endregion
    }
}