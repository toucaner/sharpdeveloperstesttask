﻿using AutoMapper;
using SharpDevelopersTestTask.Core.Domain;
using SharpDevelopersTestTask.Core.Dtos;

namespace SharpDevelopersTestTask.Core.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<Account, AccountDto>();
            CreateMap<AccountDto, Account>();
            CreateMap<Transaction, TransactionDto>()
                .ForMember(m => m.Creditor, opts => opts.MapFrom(src => src.Creditor.User.Name))
                .ForMember(m => m.Debitor, opts => opts.MapFrom(src => src.Debitor.User.Name));
            CreateMap<TransactionDto, Transaction>();
        }
    }
}