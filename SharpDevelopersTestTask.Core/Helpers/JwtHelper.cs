﻿using System.Text;

namespace SharpDevelopersTestTask.Core.Helpers
{
    public class JwtHelper
    {
        #region Methods

        public static byte[] GetSecurityKey()
        {
            return Encoding.ASCII.GetBytes(Key);
        }

        #endregion

        #region Const

        private const string Key = "SjFLi7ONtAemS2O2K32vguF7";

        #endregion
    }
}