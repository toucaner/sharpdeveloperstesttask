﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SharpDevelopersTestTask.Core.Dtos;
using SharpDevelopersTestTask.Services.Account;
using SharpDevelopersTestTask.Services.User;

namespace SharpDevelopersTestTask.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        #region Fields

        private readonly IAccountService _accountService;

        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public AccountsController(IAccountService accountService, IMapper mapper)
        {
            _accountService = accountService;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        [HttpGet("getbyid/{id}")]
        public IActionResult GetById(int id)
        {
            var account = _accountService.GetAccountById(id);
            var accountDto = _mapper.Map<AccountDto>(account);
            return Ok(accountDto);
        }

        [HttpGet("getall/{id}")]
        public IActionResult GetAll(int id)
        {
            var accounts = _accountService.GetAllAccountWithExcludeId(id);
            var accountDtos = _mapper.Map<IList<AccountDto>>(accounts);
            return Ok(accountDtos);
        }

        #endregion
    }
}