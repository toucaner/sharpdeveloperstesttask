﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SharpDevelopersTestTask.Core.Domain;
using SharpDevelopersTestTask.Core.Dtos;
using SharpDevelopersTestTask.Services.Account;
using SharpDevelopersTestTask.Services.Transaction;

namespace SharpDevelopersTestTask.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        #region Fields

        private readonly ITransactionService _transactionService;

        private readonly IAccountService _accountService;

        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public TransactionsController(ITransactionService transactionService, 
            IAccountService accountService, IMapper mapper)
        {
            _transactionService = transactionService;
            _accountService = accountService;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        [HttpGet("getbyid/{id}")]
        public IActionResult GetById(int id)
        {
            var transaction = _transactionService.GetTransactionById(id);
            var accountDto = _mapper.Map<TransactionDto>(transaction);
            return Ok(accountDto);
        }

        [HttpGet("getall/{id}")]
        public IActionResult GetAll(int id)
        {
            var transactions = _transactionService.GetAllTransactionByAccountId(id);
            var transactionDtos = _mapper.Map<IList<TransactionDto>>(transactions);
            return Ok(transactionDtos);
        }

        [HttpPost("add")]
        public IActionResult AddTransaction([FromBody] TransactionDto transactionDto)
        {
            if (!_transactionService.CanMakeTransaction(transactionDto.CreditorId, transactionDto.Amount))
                return BadRequest(new { message = "Error during transaction." });

            var transaction = _mapper.Map<Transaction>(transactionDto);
            var creditor = _accountService.GetAccountById(transactionDto.CreditorId);
            var debitor = _accountService.GetAccountById(transactionDto.DebitorId);
            transaction.Creditor = creditor;
            transaction.Debitor = debitor;

            try
            {
                _transactionService.CreateTransaction(transaction);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        #endregion
    }
}