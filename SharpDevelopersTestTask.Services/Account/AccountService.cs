﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharpDevelopersTestTask.Data.Repository;

namespace SharpDevelopersTestTask.Services.Account
{
    public class AccountService : IAccountService
    {
        #region Fields

        private readonly IRepository<Core.Domain.Account> _accountRepository;

        #endregion

        #region Constructor

        public AccountService(IRepository<Core.Domain.Account> accountRepository)
        {
            _accountRepository = accountRepository;
        }

        #endregion

        #region Methods

        public void CreateAccount(Core.Domain.Account account)
        {
            _accountRepository.Insert(account);
        }

        public Core.Domain.Account GetAccountById(int id)
        {
            return _accountRepository.Get(id);
        }

        public IEnumerable<Core.Domain.Account> GetAllAccountWithExcludeId(int exclideId)
        {
            return
                _accountRepository.Entities.AsNoTracking().Include(c => c.User).Where(t => t.Id != exclideId).ToList();

        }

        #endregion
    }
}