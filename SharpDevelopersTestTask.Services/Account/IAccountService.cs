﻿using System.Collections;
using System.Collections.Generic;

namespace SharpDevelopersTestTask.Services.Account
{
    public interface IAccountService
    {
        #region Methods

        void CreateAccount(Core.Domain.Account account);

        Core.Domain.Account GetAccountById(int id);

        IEnumerable<Core.Domain.Account> GetAllAccountWithExcludeId(int exclideId);

        #endregion
    }
}