﻿using System.Collections.Generic;
using SharpDevelopersTestTask.Core.Dtos;

namespace SharpDevelopersTestTask.Services.Transaction
{
    public interface ITransactionService
    {
        #region Methods

        Core.Domain.Transaction GetTransactionById(int id);

        IEnumerable<Core.Domain.Transaction> GetAllTransactionByAccountId(int accountId);

        bool CanMakeTransaction(int accountId, decimal amount);

        void CreateTransaction(Core.Domain.Transaction transaction);

        #endregion
    }
}