﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SharpDevelopersTestTask.Data.Repository;

namespace SharpDevelopersTestTask.Services.Transaction
{
    public class TransactionService : ITransactionService
    {
        #region Fields

        private readonly IRepository<Core.Domain.Transaction> _transactionRepository;

        private readonly IRepository<Core.Domain.Account> _accountRepository;

        #endregion

        #region Constructor

        public TransactionService(IRepository<Core.Domain.Transaction> transactionRepository,
            IRepository<Core.Domain.Account> accountRepository)
        {
            _transactionRepository = transactionRepository;
            _accountRepository = accountRepository;
        }

        #endregion

        #region Methods

        public IEnumerable<Core.Domain.Transaction> GetAllTransactionByAccountId(int accountId)
        {
            return
                _transactionRepository.Entities.AsNoTracking()
                    .Include(c => c.Creditor).ThenInclude(c => c.User)
                    .Include(c => c.Debitor).ThenInclude(c => c.User)
                    .Where(t => t.Creditor.Id == accountId || t.Debitor.Id == accountId).ToList();
        }

        public Core.Domain.Transaction GetTransactionById(int id)
        {
            return _transactionRepository.Get(id);
        }

        public bool CanMakeTransaction(int accountId, decimal amount)
        {
            var creditor = _accountRepository.Get(accountId);
            return amount <= creditor.Amount;
        }

        public void CreateTransaction(Core.Domain.Transaction transaction)
        {
            transaction.Created = DateTime.UtcNow;
            _transactionRepository.Insert(transaction);
            UpdateAccounts(transaction);
        }

        public void UpdateAccounts(Core.Domain.Transaction transaction)
        {
            var creditor = _accountRepository.Get(transaction.Creditor.Id);
            var debitor = _accountRepository.Get(transaction.Debitor.Id);
            creditor.Amount -= transaction.Amount;
            debitor.Amount += transaction.Amount;
            _accountRepository.Update(creditor);
            _accountRepository.Update(debitor);
        }

        #endregion
    }
}