﻿using System.Collections.Generic;

namespace SharpDevelopersTestTask.Services.User
{
    public interface IUserService
    {
        #region Methods

        Core.Domain.User Authenticate(string email, string password);

        Core.Domain.User CreateUser(Core.Domain.User user, string password);

        Core.Domain.User GetUserById(int id);

        IEnumerable<Core.Domain.User> GetAll();

        #endregion
    }
}